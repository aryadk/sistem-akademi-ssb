<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//siswa
$router->post('/siswa', 'siswaController@create');
$router->get('/siswa', 'siswaController@read');
$router->post('/siswa/{id}', 'siswaController@update');
$router->delete('/siswa/{id}', 'siswaController@delete');
$router->get('siswa/{id}', 'siswaController@detail');
//end

//coach
$router->post('/coach', 'coachController@create');
$router->get('/coach', 'coachController@read');
$router->post('/coach/{id}', 'coachController@update');
$router->delete('/coach/{id}', 'coachController@delete');
$router->get('coach/{id}', 'coachController@detail');
//end
