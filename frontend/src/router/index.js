import Vue from 'vue'
import Router from 'vue-router'
import siswa from '@/components/siswa'
import coach from '@/components/coach'
import siswaForm from '@/components/siswaForm'
import coachForm from '@/components/coachForm'

Vue.use(Router)

export default new Router({
	routes: [
	{
		path: '/siswa',
		name: 'siswa',
		component: siswa
	},	
	{
		path: '/siswa/create',
		name: 'siswaCreate',
		component: siswaForm
	},
	{
		path: '/siswa/:id',
		name: 'siswaEdit',
		component: siswaForm	
	},
	{
		path: '/coach',
		name: 'coach',
		component: coach
	},
	{
		path: '/coach/create',
		name: 'coachCreate',
		component: coachForm
	},
	{	
		path: '/coachform',
		name: 'coachForm',
		component: coachForm
	},
	{
		path: '/coach/:id',
		name: 'coachEdit',
		component: coachForm	
	}
	]
})