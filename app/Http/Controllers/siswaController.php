<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class siswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(), [
        'nama_lengkap' => 'required|string',
        'jenis_kelamin' => 'required|max:1',
        'alamat' => 'required|string'
        ]);

        if ($validation->fails()) {
            $errors = $validation->errors();
            return [
                'status' => 'error',
                'message' => $errors,
                'result' => null
            ];
        }

        $result = \App\siswa::create($request->all());
        if ($result) {
            return [
                'status' => 'success',
                'message' => 'Data Berhasil Ditambahkan',
                'result' => $result
            ];
        }else{
            return [
                'status' => 'error',
                'message' => 'Data Gagal Ditambahkan',
                'result' => null
            ];
        }
    }

    public function read(Request $request)
    {
        $result = \App\siswa::all();

        return [
            'status' => 'success',
            'message' => '',
            'result' => $result
        ];
    }

    public function update(Request $request, $id)
    {

        $validation = Validator::make($request->all(), [
        
        'nama_lengkap' => 'required|string',
        'jenis_kelamin' => 'required|max:1',
        'alamat' => 'required|string'
        ]);

        if ($validation->fails()) {
            $errors = $validation->errors();
            return [
                'status' => 'error',
                'message' => $errors,
                'result' => null
            ];
        }

        $siswa = \App\siswa::find($id);
        if (empty($siswa)) {
            return [
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
                'result' => null  
            ];
        }

        $result = $siswa->update($request->all());
        if ($result) {
            return [
                'status' => 'success',
                'message' => 'Data Berhasil Diubah',
                'result' => $result
            ];
        }else{
            return [
                'status' => 'error',
                'message' => 'Data Gagal Diubah',
                'result' => null
            ];
        }

    }

    public function delete(Request $request, $id)
    {
        $siswa = \App\siswa::find($id);
        if (empty($siswa)) {
            return [
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
                'result' => null  
            ];
        }

        $result = $siswa->delete($id);
        if($result){
            return[
                'status' => 'success',
                'message' => 'Data Berhasil Dihapus',
                'result' => $result
            ];
        }else {
            return [
                'status' => 'success',
                'message' => 'Data gagal Dihapus',
                'result' => null
            ];
        }

    }

    public function detail($id)
    {
        $siswa = \App\siswa::find($id);

        if (empty($siswa)) {
            return[
                'status'  => 'error',
                'message' => 'Data Tidak Di temukan',
                'result' => null
            ];
        }

        return[

                'status'  => 'Success',
                'result' => $siswa
        ];
    }

}
