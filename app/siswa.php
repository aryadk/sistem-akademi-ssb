<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */

class siswa extends Model
{
	public $table = 'siswa';

	protected $fillable = [
		'nama_lengkap',
		'tgl_lahir',
		'jenis_kelamin',
		'alamat'
	];
}